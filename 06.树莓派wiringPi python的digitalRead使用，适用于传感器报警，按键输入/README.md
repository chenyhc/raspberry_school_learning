# 06.树莓派wiringPi python的digitalRead使用，适用于传感器报警，按键输入

测试digitalRead功能，用于读取某个GPIO的状态，是HIGH还是LOW，常用于各种数字开关量传感器的读取判定比如【压力感应、可燃性气体超限报警，土壤湿度传感器、霍尔传感器、光电传感器、麦克风传感器等等各种开关量传感器读取】，按键读取等等。

常用传感器可以在下面链接搜索购买：https://ilovemcu.taobao.com

其中比较重要的3个部分代码

1.gpio.wiringPiSetupGpio()  # For GPIO pin numbering  使用BCM编号

为了和pinout还有Fritzing绘图时候看编号方便，以后使用BCM，不用wPi的编码了

2.gpio.pinMode(SENSOR_PIN, GPIO.INPUT)

用于设置IO的属性为输入

3.gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_UP)

设置输入状态是：输入悬空GPIO.PUD_OFF，输入上拉GPIO.PUD_UP，输入下拉GPIO.PUD_DOWN

以下就是一个简单的某种开关量传感器低电平触发报警的参考例程

```
import wiringpi as gpio
from wiringpi import GPIO
SENSOR_PIN = 23

# One of the following MUST be called before using IO functions:
gpio.wiringPiSetupGpio()  # For GPIO pin numbering  使用BCM编号

gpio.pinMode(SENSOR_PIN, GPIO.INPUT)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_OFF)
gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_UP)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_DOWN)
while 1:
    state = gpio.digitalRead(SENSOR_PIN)
    print(state)
    if state == GPIO.LOW :
        print("SENSOR LOW ALARM")
   
    gpio.delay(500)


```

再提供一个按键的例程供学习，

其中scanKey(KEY_PIN)，可以用于检测普通按键输入，通俗讲就是按下一次，算一次。

scanKey(KEY_PIN,1)，可以用于连续按键输入，通俗讲就是按着不松开，当多次按下

```
import wiringpi as gpio
from wiringpi import GPIO
KEY_PIN = 16

KEY_UP = True
count = 0

def scanKey(PIN,mode = 0):
    global KEY_UP
    if mode == 1:
        KEY_UP = True
    if( KEY_UP == True and gpio.digitalRead(PIN) == GPIO.LOW):
        KEY_UP = False
        gpio.delay(20)
        if(gpio.digitalRead(PIN) == GPIO.LOW):
            return 1
    #key release      
    if( KEY_UP == False and gpio.digitalRead(PIN) == GPIO.HIGH):
        KEY_UP = True
   
    return 0
                    

# One of the following MUST be called before using IO functions:
gpio.wiringPiSetupGpio()  # For GPIO pin numbering  使用BCM编号

gpio.pinMode(KEY_PIN, GPIO.INPUT)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_OFF)
gpio.pullUpDnControl(KEY_PIN, GPIO.PUD_UP)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_DOWN)
while 1:
    state = scanKey(KEY_PIN)
    if state == 1 :
        count = count + 1
        print("press",count)
```

以上例程配套的接线图

![](https://img.alicdn.com/imgextra/i3/63891318/O1CN01HWG3Hy1LbgbNtEw9U_!!63891318.png)

