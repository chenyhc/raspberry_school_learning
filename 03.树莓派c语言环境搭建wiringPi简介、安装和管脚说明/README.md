

# 03.c语言控制IO，wiringPi简介、安装和管脚说明，root权限

## 1.WiringPi简介
​	WiringPi是应用于树莓派平台的GPIO控制库函数，WiringPi遵守GUN Lv3。wiringPi使用C或者C++开发并且可以被其他语言包转，例如python、ruby或者PHP等。WiringPi中的函数类似于Arduino的wiring系统，这使得熟悉arduino的用户使用wringPi更为方便。
树莓派具有26个普通输入和输出引脚。在这26个引脚中具有8个普通输入和输出管脚，这8个引脚既可以作为输入管脚也可以作为输出管脚。除此之外，树莓派还有一个2线形式的I2C、一个4线形式的SPI和一个UART接口。树莓派上的I2C和SPI接口也可以作为普通端口使用。如果串口控制台被关闭便可以使用树莓派上的UART功能。如果不使用I2C，SPI和UART等复用接口，那么树莓派总共具有8+2+5+2 =17个普通IO。wiringPi包括一套gpio控制命令，使用gpio命令可以控制树莓派GPIO管脚。用户可以利用gpio命令通过shell脚本控制或查询GPIO管脚。wiringPi是可以扩展的，可以利用wiringPi的内部模块扩展模拟量输入芯片，可以使用MCP23x17/MCP23x08（I2C 或者SPI）扩展GPIO接口。另外可通过树莓派上的串口和Atmega（例如arduino等）扩展更多的GPIO功能。另外，用户可以自己编写扩展模块并把自定义的扩展模块集成到wiringPi中。WiringPi支持模拟量的读取和设置功能，不过在树莓派上并没有模拟量设备。但是使用WiringPi中的软件模块却可以轻松地应用AD或DA芯片。

## 2.wiringPi安装
​	wiringPi的安装存在方案A和方案B。wiringPi使用GIT工具维护个更新代码，但是如果处于一些其他原因不能使用GIT，那么也可以使用方案B下载和安装wiringPi。
方案A——使用GIT工具
如果在你的平台上还没有安装GIT工具，可以输入以下命令：

```
sudo apt-get install git-core
```


如果在这个过程中出现错误，尝试更新软件，例如输入以下指令：

```
sudo apt-get update
sudo apt-get upgrade
```

紧接着可以通过GIT获得wiringPi的源代码

```
git clone git://git.drogon.net/wiringPi
```

出现问题1：上面指令我测试被拒绝，

![](https://img.alicdn.com/imgextra/i4/63891318/O1CN015DGvbJ1LbgbLkPUEK_!!63891318.png)

网上查询了一个github的链接：https://github.com/WiringPi/WiringPi.git

使用下面的指令直接git一个，安装成功

```
sudo git clone https://github.com/WiringPi/WiringPi.git
```

![](https://img.alicdn.com/imgextra/i4/63891318/O1CN01ASOmIX1LbgbNpY9zR_!!63891318.png)

若需要更新wiringPi。

```
cd wiringPi
git pull origin
```

进入wiringPi目录并安装wiringPi

```
cd wiringPi
./build
```

build脚本会帮助你编译和安装wiringPi

出现问题2：文件权限问题

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01l5rLlQ1LbgbPCNsZ0_!!63891318.png)

然后开始研究如何解锁root权限

```
https://www.jianshu.com/p/66ad2197e1ed
```

在root账户内重新build，终于安装成功了！！！

![](https://img.alicdn.com/imgextra/i2/63891318/O1CN01PmCPgv1LbgbLcwntR_!!63891318.png)

然后还是在pi这个账户内操作试试，输入gpio -v

![](https://img.alicdn.com/imgextra/i4/63891318/O1CN01MFq4Lp1LbgbNfy5Km_!!63891318.png)

通过gpio readall可以看到wPi对应的pinOut，和昨天BCM的不一样，这里是25，昨天是26

然后touch led.c新建一个c文件

使用vi打开led.c内容输入

```
#include <wiringPi.h>
int main(void)
{
    wiringPiSetup();
    pinMode (25, OUTPUT);
    for(;;)
    {
        digitalWrite(25, HIGH); delay (500);
        digitalWrite(25, LOW); delay (500);
    }
}
```

ESC后输入输入冒号，进入命令号，输入wq保存退出！！这里去学习了下vi的操作，哎，到处都是坑啊，不会就要学！！！谁让咱不会

接下来开始编译c文件，使用如下指令

```
sudo gcc -Wall led.c -o led -l wiringPi
```

编译成功后，以下指令运行

```
./led
```

好的，终于可以使用c语言点亮LED了，而且这个代码很熟悉，因为懂arduino啊！！！

————————————————

LED连接方式同上一节，不再过多说明
![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01tYLujY1LbgbLEW6zj_!!63891318.png)



然后，总觉得VI写代码好难受，然后又切回图形界面，发现自带一个Geany的编辑环境，可以很容易写c，但是编译时候还是要输入指令，所以新建了makefile文件来做make操作。最终也通过这个环境编译运行成功c语言。终于不用受vi的苦了！！

![](https://img.alicdn.com/imgextra/i1/63891318/O1CN01PqhNl01LbgbNtKKK0_!!63891318.png)

具体这个Geany的操作不再截图了，很简单。或者可以参考我的视频操作吧！

